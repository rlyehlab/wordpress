# Rlyeh Wordpress

This is our Wordpress implementation based on the official docker image of Wordpress with PHP-FPM plus nginx, both over alpine linux.

## Requirements

* Docker
* Docker Compose

## Usage

1. Clone.
1. Create `.env` file (edit the file to your needs):

    ```
    cp env .env
    DBPASS="$(tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=60 count=1 iflag=fullblock status=none 2> /dev/null)"
    sed -i "s/WORDPRESS_DB_PASSWORD=/WORDPRESS_DB_PASSWORD=${DBPASS}/" .env
    sed -i "s/MYSQL_PASSWORD=/MYSQL_PASSWORD=${DBPASS}/" .env
    sed -i "s/MYSQL_ROOT_PASSWORD=/MYSQL_ROOT_PASSWORD=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=60 count=1 iflag=fullblock status=none 2> /dev/null)/" .env
    #
    # Optionally, if you also want the rest of the values to be random:
    #
    DBUSER="$(tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=12 count=1 iflag=fullblock status=none 2> /dev/null)"
    sed -i "s/MYSQL_USER=/MYSQL_USER=${DBUSER}/" .env
    sed -i "s/WORDPRESS_DB_USER=/WORDPRESS_DB_USER=${DBUSER}/" .env
    DBNAME="$(tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=12 count=1 iflag=fullblock status=none 2> /dev/null)"
    sed -i "s/MYSQL_DATABASE=/MYSQL_DATABASE=${DBNAME}/" .env
    sed -i "s/WORDPRESS_DB_NAME=/WORDPRESS_DB_NAME=${DBNAME}/" .env
    sed -i "s/WORDPRESS_TABLE_PREFIX=/WORDPRESS_TABLE_PREFIX=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | dd bs=4 count=1 iflag=fullblock status=none 2> /dev/null)/" .env
    ```

1. Check settings: `docker-compose config`. Edit as necessary.
1. Pull: `docker-compose pull`.
1. Run: `docker-compose up -d`.

By default, it listens at 0.0.0.0:8080 (plain).  
Currently, TLS should be managed by a reverse proxy.

## Backup

Copy/tar both `mysql` and `wordpress` directories.

## Upgrade

1. Make sure you have a backup.
1. Put it all down: `docker-compose down`.
  * *Optional*: remove docker images to free up some space.
1. Pull new images: `docker-compose pull`
1. Put it up again: `docker-compose up -d`
1. For wordress: log in to the admin panel to complete update steps. MariaDB
   requires no additional steps.

## License

**Rlyeh Wordpress** is made by [HacKan](https://hackan.net) under GNU GPL v3.0+. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

    Copyright (C) 2017 HacKan (https://hackan.net)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

